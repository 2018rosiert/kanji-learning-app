<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add new kanji</title>

<link rel="stylesheet" href="css/style2.css" type="text/css" />
</head>

<body>
	<h1>Choose the level of the new Kanji</h1>
	<form method="Post" action="./validerChoixNiveau">
		<table>
			<tr>
				<td>Level (1-50)</td>
				<td><input type="number" name="Niveau"/></td>
		</table>
		<table>
			<tr>
				<!-- submit : un bouton qui valide le formulaire -->
				<td><input type="submit" name="action" value="Submit" /></td>
			</tr>
			<tr>
				<td><a href="<c:url value="/Homepage.jsp"/>">Back to homepage</a></td>
			</tr>
		</table>
	</form>
</body>

</html>