<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Exercises</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/style1.css' />" />
</head>
<body>
	<p>${exception}</p>
	<div id="menu">
		<form method="Post" action="./writing">
			<h1>Writing exercise</h1>
			<td>Max writing score (1-5)</td>
			<td><input type="number" name="scoreMax" /></td>
			<td><input type="submit" name="action" value="Submit" /></td>
		</form>

		<form method="Post" action="./reading">
			<h1>Reading exercise</h1>
			<td>Max reading score (1-5)</td>
			<td><input type="number" name="scoreMax" /></td>
			<td><input type="submit" name="action" value="Submit" /></td>
		</form>

		<form method="Post" action="./meaning">
			<h1>Meaning exercise</h1>
			<td>Max meaning score (1-5)</td>
			<td><input type="number" name="scoreMax" /></td>
			<td><input type="submit" name="action" value="Submit" /></td>
		</form>
	</div>
	<p></p>
	<a href="<c:url value="/Homepage.jsp"/>">Back to homepage</a>
</body>
</html>