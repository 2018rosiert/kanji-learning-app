<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="<c:url value='/css/style1.css' />"/>
</head>
<body>
	<h1>&#x6F22;&#x5B57; - Kanji Learning Website</h1>
		<p>
			<a href="<c:url value="/listKanji"/>">1. Display the Kanji database</a>
		</p>
		<p>
			<a href="<c:url value="/addNewKanji"/>">2. Add new Kanji</a>
		</p>
		<p>
			<a href="<c:url value="/exercises"/>">3. Do an exercise</a>
		</p>
</body>
</html>