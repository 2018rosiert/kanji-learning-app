<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/style2.css" type="text/css" />
</head>
<body>
	<h1>Kanji list</h1>
	<table border="1">
		<th>Writing</th>
		<th>Onyomi</th>
		<th>Kunyomi</th>
		<th>Meaning</th>
		<c:forEach items="${TousLesKanjis}" var="k">
			<tr>
				<td>${k.writing}</td>
				<td>${k.onyomi}</td>
				<td>${k.kunyomi}</td>
				<td>${k.meaning}</td>
				<td><a href="<c:url value="eraseKanji?idHex=${fn:substring(k.writing, 3, 7)}"/>">Erase</a></td>
				<td><a href="<c:url value="displaySimpleKanji?idHex=${fn:substring(k.writing, 3, 7)}"/>">Display page</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="<c:url value="/Homepage.jsp"/>">Back to homepage</a>
</body>
</html>