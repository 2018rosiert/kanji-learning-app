<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="javax.swing.JOptionPane"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="modele.Kanji"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="css/style2.css" type="text/css" />
</head>
<body>

	<title>Reading exercise</title>
	<h1>Reading exercise</h1>
	<p>What is the reading of  ${writingHex} / ${meaning} ?</p>

	<form method="post" action="./selection">
		<select name="selection">
			<c:forEach items="${choices}" var="choice">

				<option value="${choice}">${choice}</option>

			</c:forEach>

		</select> 
		<input type="submit" value="submit" />
		<input type="hidden" name="answer" value="${answer}">
		<input type="hidden" name="exercise" value="reading">
		<input type="hidden" name="keyHex" value="${fn:substring(writingHex, 3, 7)}">
		<input type="hidden" name="scoreMax" value="${scoreMax}">
	</form>
</body>
</html>