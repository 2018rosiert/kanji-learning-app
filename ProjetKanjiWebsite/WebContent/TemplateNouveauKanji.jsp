<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
			@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,
			.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}
			.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}
		</style>
		<meta charset="utf-8">
		<meta name="viewport" content="width=1210, initial-scale=0.8">
		<meta name="description" content="Kanji alive is a free web application designed to 
		help Japanese language students of all levels learn to read and write kanji.">
		<title>Kanji alive Web Application</title>
		<base href="/">
	</head>
	
	<body>
		<!-- ngView: -->
		<h3 id="writing">Writing</h3>
		<p class="writing ng-binding">Click on the image and press SPACE bar to display stroke order</p>
		<div class="container-typeface-video" ng-switch="detailMode">
			<!-- ngSwitchDefault: -->
			<div class="video-container ng-scope" ng-switch-default="">
				<div class="video">
					<c:set var="videoURI" value="${videoURI}"/> 
					<video ng-click="setDetailMode('typeface')" ng-attr-poster="{{doc.poster_video_source}}" 
					playsinline="" vg-src="sources" preload="metadata" vg-video="" 
					poster="${videoURI.poster}" 
					src="${videoURI.mp4}"
					type="video/mp4"></video>
				</div>
			</div>
			<!-- end ngSwitchWhen: -->
			<!-- ngSwitchWhen: typeface -->
		</div>

		<c:set var="k" value="${Kanji}"/>  
		<h3 class="meaning">Meaning</h3>
		<p>${k.meaning}</p>
		<h3 class="onyomi">Onyomi</h3>
		<p>${k.onyomi}</p>
		<h3 class="kunyomi">Kunyomi</h3>
		<p>${k.kunyomi}</p>
		
		<div id="menu">
			<p>
				<a href="<c:url value="/listKanji"/>">Back to Kanji list</a>
			</p>
		</div>

	</body>
</html>

