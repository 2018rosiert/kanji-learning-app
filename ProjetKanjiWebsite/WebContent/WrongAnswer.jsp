<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Wrong answer !</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/style1.css' />" />
</head>
<body>
	<h1>Wrong answer !</h1>
	<p>Your ${exercise} score for this Kanji is now	${nouveauScore}</p>
	<a href="<c:url value="/${exercise}?scoreMax=${scoreMax}"/>">Next</a>
	<p></p>
	<a href="<c:url value="/Homepage.jsp"/>">Back to homepage</a>
</body>
</html>