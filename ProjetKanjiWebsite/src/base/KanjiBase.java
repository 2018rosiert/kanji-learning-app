package base;

import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.swing.JOptionPane;

import modele.Kanji;

public class KanjiBase {
	private Vector<Kanji> lesKanjis;
	private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tx;

	public KanjiBase(){
		System.out.println("ok");
		emf = Persistence.createEntityManagerFactory("UsersDB");
		
		//Kanji k = new Kanji("wr3", "un3", "ku3", "me3");
		//ajouterKanji(k);
		
		//getListeKanjis();
	}

	/*
	 *  Ajoute un contact et retourne le contact ajoutÃ© au carnet d'adresses
	 */
	public Kanji ajouterKanji(Kanji k) {
		System.out.println("---------- CrÃ©ation d'un kanji -----------");

		// CrÃ©ation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();
		tx = em.getTransaction();

		// DÃ©but de la transaction
		tx.begin();

		// On place dans le contexte de persistance un objet contact en  vue 
		// d'une future insertion dans la table contact (prÃ©paration 
		// d'une requÃªte SQL INSERT)
		System.out.println(k);
		em.persist(k);

		// Fin de la transaction : l'objet c est copiÃ© dans la  
		// table contact de la base de donnÃ©es carnet d'adresses
		// (ExÃ©cution de la requÃªte SQL INSERT)
		tx.commit();

		// Fermeture du contexte de persistence
		em.close();

		return k;
	}

	/*
	 *  Supprimer un contact 
	 */
	public Kanji supprimerKanji(String writing) {
		System.out.println("---------- Suppression d'un kanji -----------");

		// CrÃ©ation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();
		tx = em.getTransaction();

		// On place l'objet c dans le contexte de persistence
		Kanji k = em.find(Kanji.class, writing);

		// DÃ©but de la transaction
		tx.begin();

		// on supprime l'objet persistÃ© c
		em.remove(k);

		// Fin de la transaction : l'objet c est supprimÃ© de la  
		// table contact de la base de donnÃ©es carnet d'adresses 
		// (ExÃ©cution de la requÃªte SQL DELETE)
		tx.commit();

		// Fermeture du contexte de persistence
		em.close();
		
		return k;
	}

	// Renvoie un kanji identifié par son écriture
	public Kanji getKanji(String writing) {
		System.out.println("---------- Obtention d'un kanji par son Ã©criture -----------");

		// Creation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();

		// On place l'objet k dans le contexte de persistence
		Kanji k = em.find(Kanji.class, writing);

		// Fermeture du contexte de persistence
		em.close();
		
		return k;
	}


	// Renvoie tous les kanjis de la base de donn�es
	public List<Kanji> getListeKanjis() {
		System.out.println("---------- Lecture de tous les kanjis  (avec createNamedQuery) -----------");

		// CrÃ©ation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();

		// Construction d'un objet Query Ã  l'aide de la requÃªte NommÃ©e Contact.findAll dÃ©finie
		// dans la classe Contact
		Query q = em.createNamedQuery("Kanji.findAll");

		// ExÃ©cution de la requÃªte et rÃ©cupÃ©ration de la liste de rÃ©sulats
		List<Kanji> lesKanjis = q.getResultList();

		// Affichage des rÃ©sultats
		for (Kanji unKanji : lesKanjis) {
			System.out.println(unKanji);
		}

		// Fermeture du contexte de persistence
		em.close();
		return lesKanjis;
	}	
	
	public List<Kanji> getKanjisScoreWriting(int scoreWriting) {
		// CrÃ©ation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();

		// Construction d'un objet Query Ã  l'aide de la requÃªte NommÃ©e Contact.findAll dÃ©finie
		// dans la classe Contact
		Query q = em.createNamedQuery("Kanji.findByScoreWriting");
		q.setParameter("scoreWriting", scoreWriting);

		// ExÃ©cution de la requÃªte et rÃ©cupÃ©ration de la liste de rÃ©sulats
		List<Kanji> lesKanjis = q.getResultList();

		// Affichage des rÃ©sultats
		for (Kanji unKanji : lesKanjis) {
			System.out.println(unKanji);
		}

		// Fermeture du contexte de persistence
		em.close();
		return lesKanjis;
	}
	
	public List<Kanji> getKanjisScoreMeaning(int scoreMeaning) {
		// CrÃ©ation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();

		// Construction d'un objet Query Ã  l'aide de la requÃªte NommÃ©e Contact.findAll dÃ©finie
		// dans la classe Contact
		Query q = em.createNamedQuery("Kanji.findByScoreMeaning");
		q.setParameter("scoreMeaning", scoreMeaning);

		// ExÃ©cution de la requÃªte et rÃ©cupÃ©ration de la liste de rÃ©sulats
		List<Kanji> lesKanjis = q.getResultList();

		// Affichage des rÃ©sultats
		for (Kanji unKanji : lesKanjis) {
			System.out.println(unKanji);
		}

		// Fermeture du contexte de persistence
		em.close();
		return lesKanjis;
	}
	
	public List<Kanji> getKanjisScoreReading(int scoreReading) {
		// CrÃ©ation d'un contexte de persistence et d'une transaction
		em = emf.createEntityManager();

		// Construction d'un objet Query Ã  l'aide de la requÃªte NommÃ©e Contact.findAll dÃ©finie
		// dans la classe Contact
		Query q = em.createNamedQuery("Kanji.findByScoreReading");
		q.setParameter("scoreReading", scoreReading);

		// ExÃ©cution de la requÃªte et rÃ©cupÃ©ration de la liste de rÃ©sulats
		List<Kanji> lesKanjis = q.getResultList();

		// Affichage des rÃ©sultats
		for (Kanji unKanji : lesKanjis) {
			System.out.println(unKanji);
		}

		// Fermeture du contexte de persistence
		em.close();
		return lesKanjis;
	}
	
	public void increaseScoreWriting(String key) {
		Kanji k = getKanji(key);
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		em.detach(k);
		k.increaseScoreWriting();

		em.merge(k);
		tx.commit();
		em.close();
	}	
	
	public void decreaseScoreWriting(String key) {
		Kanji k = getKanji(key);
		
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		k.decreaseScoreWriting();

		em.merge(k);
		tx.commit();
		em.close();
	}
	
	public void increaseScoreMeaning(String key) {
		Kanji k = getKanji(key);
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		em.detach(k);
		k.increaseScoreMeaning();

		em.merge(k);
		tx.commit();
		em.close();
	}	
	
	public void decreaseScoreMeaning(String key) {
		Kanji k = getKanji(key);
		
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		k.decreaseScoreMeaning();

		em.merge(k);
		tx.commit();
		em.close();
	}
	
	public void increaseScoreReading(String key) {
		Kanji k = getKanji(key);
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		em.detach(k);
		k.increaseScoreReading();

		em.merge(k);
		tx.commit();
		em.close();
	}	
	
	public void decreaseScoreReading(String key) {
		Kanji k = getKanji(key);
		
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		k.decreaseScoreReading();

		em.merge(k);
		tx.commit();
		em.close();
	}
	
	
	
	public static void main(String[] args) {
		new KanjiBase();
	}

}
