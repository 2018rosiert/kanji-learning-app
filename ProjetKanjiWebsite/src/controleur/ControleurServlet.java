package controleur;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import base.KanjiBase;
import modele.Kanji;
import modele.UTF16HexConversion;

import org.openapitools.client.ApiException;
import org.openapitools.client.api.KanjiAliveApi;
import org.openapitools.client.model.KanjiDetails;
import org.openapitools.client.model.KanjiDetailsKanjiVideo;
import org.openapitools.client.model.KanjiObject;

/**
 * Servlet implementation class ControleurServlet
 */
@WebServlet("/ControleurServlet")
public class ControleurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private KanjiBase base;
    private KanjiAliveApi api;
    private String key;
    private String host;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControleurServlet() {
        super();
        // TODO Auto-generated constructor stub
        base = new KanjiBase();
		key = "f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee";
		host = "kanjialive-api.p.rapidapi.com";
		api = new KanjiAliveApi();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String action = request.getServletPath();
		
		switch (action) {
		case "/addNewKanji":
			doGetAjouterNouveauxKanji(request, response);
			break;
		case "/exercises":
			doGetExercises(request, response);
			break;
		case "/listKanji":
			doGetListerKanji(request, response);
			break;
		case "/eraseKanji":	
			doGetSupprimerKanji(request, response);
			break;
		case "/displaySimpleKanji":		
			doGetAfficherSimpleKanji(request, response);
			break;
		case "/writing":
			doGetWriting(request, response);
			break;
		case "/meaning":
			doGetMeaning(request, response);
			break;
		case "/reading":
			doGetReading(request, response);
			break;
		default:
			break;

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String action = request.getServletPath();

		switch (action) {
		
		case "/validerChoixNiveau":
			doPostChoixNiveau(request, response);
			break;
			
		case "/selection":
			doPostSelection(request, response);
			break;
		default:
			break;
		}
	}
	
	
	protected void doGetListerKanji(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Kanji> lesKanjis = base.getListeKanjis();
		for (Kanji unKanji : lesKanjis) {
			UTF16HexConversion convWrt = new UTF16HexConversion(unKanji.getWriting());
			unKanji.setWriting(convWrt.getHexStr());

			UTF16HexConversion convOny = new UTF16HexConversion(unKanji.getOnyomi());
			unKanji.setOnyomi(convOny.getHexStr());

			UTF16HexConversion convKun = new UTF16HexConversion(unKanji.getKunyomi());
			unKanji.setKunyomi(convKun.getHexStr());
		}
		request.setAttribute("TousLesKanjis", lesKanjis);
		
		request.getRequestDispatcher("ListKanji.jsp").forward(request, response);
	}
	
	
	protected void doGetSupprimerKanji(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String writingHex = request.getParameter("idHex");
		
		UTF16HexConversion conv = new UTF16HexConversion(writingHex);
		String writing = conv.getUTF16Str();

		base.supprimerKanji(writing);
		doGetListerKanji(request, response);
	}
	
	
	protected void doGetAfficherSimpleKanji(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String writingHex = request.getParameter("idHex");
		
		UTF16HexConversion conv = new UTF16HexConversion(writingHex);
		String writing = conv.getUTF16Str();

		Kanji K = base.getKanji(writing);
		
		//Settings for display of Kunyomi and Onyomi
		UTF16HexConversion convOny = new UTF16HexConversion(K.getOnyomi());
		K.setOnyomi(convOny.getHexStr());

		UTF16HexConversion convKun = new UTF16HexConversion(K.getKunyomi());
		K.setKunyomi(convKun.getHexStr());
		
		request.setAttribute("Kanji",K);
		
		//Settings for display of the video
		try {
			KanjiDetails details = api.getKanjiDetails(writing, key, host);
			KanjiDetailsKanjiVideo video = details.getKanji().getVideo();
			request.setAttribute("videoURI",video);
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		request.getRequestDispatcher("TemplateNouveauKanji.jsp").forward(request, response);
	}
	
	
	protected void doGetAjouterNouveauxKanji(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("AddNewKanji.jsp").forward(request, response);
	}
	
	
	protected void doGetExercises(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Exercises.jsp").forward(request, response);
	}
	
	
	protected void doGetWriting(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Sélection des kanjis dont le scoreWriting est inférieur à une certaine valeur
		int scoreMax = Integer.valueOf(request.getParameter("scoreMax"));
		List<Kanji> l = base.getKanjisScoreWriting(scoreMax);
		
		if (l.isEmpty()) {
			String exception = "WARNING ! No Kanji available with that score";
			request.setAttribute("exception", exception);
			request.getRequestDispatcher("Exercises.jsp").forward(request, response);
		} else {
		    Random rand = new Random();
		    
		    // Choix d'un de manière aléatoire
		    Kanji answer = l.get(rand.nextInt(l.size()));
	
		    // Tirage des autres propositions parmi tous les kanjis
			ArrayList<Kanji> choicesKanjis = new ArrayList<Kanji>();
		    l = base.getListeKanjis();
		    int nbChoix = 5;
		    
		    // Construction puis mélange de la liste des indices de 0 à la taille de la base -1
		    ArrayList<Integer> indices = new ArrayList<Integer>();
		    for (int i = 0 ; i <l.size() ; i++) {
		    	indices.add(i);
		    }
		    Collections.shuffle(indices);
		    
		    // Sélection de (nbChoix-1) kanjis suivant les indices dans la liste mélangée
		    for (int i = 1 ; i < nbChoix ; i++) {
		    	String writingid = l.get(indices.get(i)).getWriting();
		    	if (!writingid.equals(answer.getWriting())) {
		    		choicesKanjis.add(l.get(indices.get(i)));
		    	}
		    }
			
			ArrayList<String> choices = new ArrayList<String>();
			for (Kanji k : choicesKanjis) {
				UTF16HexConversion convWriting = new UTF16HexConversion(k.getWriting());
				choices.add(convWriting.getHexStr());
			}
			UTF16HexConversion convAnswer = new UTF16HexConversion(answer.getWriting());
			String answerHex = convAnswer.getHexStr();
			choices.add(answerHex);
			Collections.shuffle(choices);
			
			UTF16HexConversion convOny = new UTF16HexConversion(answer.getOnyomi());
			String onyomiHex = convOny.getHexStr();
	
			UTF16HexConversion convKun = new UTF16HexConversion(answer.getKunyomi());
			String kunyomiHex = convKun.getHexStr();
			
			System.out.println("Answer before request is " + answerHex);
			request.setAttribute("choices", choices);
			request.setAttribute("answer", answerHex);
			request.setAttribute("onyomiHex", onyomiHex);
			request.setAttribute("kunyomiHex", kunyomiHex);
			request.setAttribute("meaning", answer.getMeaning());
			request.setAttribute("scoreMax", request.getParameter("scoreMax"));
			request.getRequestDispatcher("Writing.jsp").forward(request, response);
		}
	}
	
	
	protected void doGetMeaning(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Sélection des kanjis dont le scoreMeaning est inférieur à une certaine valeur
		int scoreMax = Integer.valueOf(request.getParameter("scoreMax"));
		List<Kanji> l = base.getKanjisScoreMeaning(scoreMax);
		
		if (l.isEmpty()) {
			String exception = "WARNING ! No Kanji available with that score";
			request.setAttribute("exception", exception);
			request.getRequestDispatcher("Exercises.jsp").forward(request, response);
		} else {
			Random rand = new Random();
			// Choix d'un de manière aléatoire
			Kanji answer = l.get(rand.nextInt(l.size()));
	
			// Tirage des autres propositions parmi tous les kanjis
	
			ArrayList<Kanji> choicesKanjis = new ArrayList<Kanji>();
			l = base.getListeKanjis();
			int nbChoix = 5;
			// Construction puis mélange de la liste des indices de 0 à la taille de la base -1
			ArrayList<Integer> indices = new ArrayList<Integer>();
			for (int i = 0 ; i <l.size() ; i++) {
				indices.add(i);
			}
			Collections.shuffle(indices);
			// Sélection de (nbChoix-1) kanjis suivant les indices dans la liste mélangée
			for (int i = 1 ; i < nbChoix ; i++) {
				choicesKanjis.add(l.get(indices.get(i)));
			}
	
			ArrayList<String> choices = new ArrayList<String>();
			for (Kanji k : choicesKanjis) {
				String writingid = k.getWriting();
		    	if (!writingid.equals(answer.getWriting())) {
		    		choices.add(k.getMeaning());
		    	}
			}
			choices.add(answer.getMeaning());
			Collections.shuffle(choices);
			
			UTF16HexConversion convWriting = new UTF16HexConversion(answer.getWriting());
			String writingHex = convWriting.getHexStr();
	
			UTF16HexConversion convOny = new UTF16HexConversion(answer.getOnyomi());
			String onyomiHex = convOny.getHexStr();
	
			UTF16HexConversion convKun = new UTF16HexConversion(answer.getKunyomi());
			String kunyomiHex = convKun.getHexStr();
	
			request.setAttribute("choices", choices);
			request.setAttribute("answer", answer.getMeaning());
			request.setAttribute("onyomiHex", onyomiHex);
			request.setAttribute("kunyomiHex", kunyomiHex);
			request.setAttribute("writingHex", writingHex);
			request.setAttribute("scoreMax", request.getParameter("scoreMax"));
			request.getRequestDispatcher("Meaning.jsp").forward(request, response);
		}
	}
	
	
	protected void doGetReading(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Sélection des kanjis dont le scoreReading est inférieur à une certaine valeur
		int scoreMax = Integer.valueOf(request.getParameter("scoreMax"));
		List<Kanji> l = base.getKanjisScoreReading(scoreMax);
		
		if (l.isEmpty()) {
			String exception = "WARNING ! No Kanji available with that score";
			request.setAttribute("exception", exception);
			request.getRequestDispatcher("Exercises.jsp").forward(request, response);
		} else {
			Random rand = new Random();
			// Choix d'un de manière aléatoire
			Kanji answer = l.get(rand.nextInt(l.size()));
	
			// Tirage des autres propositions parmi tous les kanjis
	
			ArrayList<Kanji> choicesKanjis = new ArrayList<Kanji>();
			l = base.getListeKanjis();
			int nbChoix = 5;
			// Construction puis mélange de la liste des indices de 0 à la taille de la base -1
			ArrayList<Integer> indices = new ArrayList<Integer>();
			for (int i = 0 ; i <l.size() ; i++) {
				indices.add(i);
			}
			Collections.shuffle(indices);
			// Sélection de (nbChoix-1) kanjis suivant les indices dans la liste mélangée
			for (int i = 1 ; i < nbChoix ; i++) {
				choicesKanjis.add(l.get(indices.get(i)));
			}
	
			ArrayList<String> choices = new ArrayList<String>();
			for (Kanji k : choicesKanjis) {
				String writingid = k.getWriting();
		    	if (!writingid.equals(answer.getWriting())) {
					UTF16HexConversion convOny = new UTF16HexConversion(k.getOnyomi());
					String onyomiHex = convOny.getHexStr();
		
					UTF16HexConversion convKun = new UTF16HexConversion(k.getKunyomi());
					String kunyomiHex = convKun.getHexStr();
					
					choices.add(onyomiHex+" / "+kunyomiHex);
		    	}
			}
	
			UTF16HexConversion convAO = new UTF16HexConversion(answer.getOnyomi());
			String aOHex = convAO.getHexStr();
	
			UTF16HexConversion convAK = new UTF16HexConversion(answer.getKunyomi());
			String aKHex = convAK.getHexStr();
			
			choices.add(aOHex+" / "+aKHex);
			Collections.shuffle(choices);
			
			UTF16HexConversion convWriting = new UTF16HexConversion(answer.getWriting());
			String writingHex = convWriting.getHexStr();
	
			request.setAttribute("choices", choices);
			request.setAttribute("answer", aOHex+" / "+aKHex);
			request.setAttribute("writingHex", writingHex);
			request.setAttribute("meaning", answer.getMeaning());
			request.setAttribute("scoreMax", request.getParameter("scoreMax"));
			request.getRequestDispatcher("Reading.jsp").forward(request, response);
		}
	}
	
	
	protected void doPostChoixNiveau(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lvlStr = request.getParameter("Niveau");		
		String lvlrequest = "ap:c" + lvlStr;

		try {
			
			List<KanjiObject> list = api.getKanjiList(lvlrequest, key, host);
			for (KanjiObject KO : list) {

				//Récupération des écritures via l'API Kanji Alive
				String writing = KO.getKanji().getCharacter();
				
                //Récupération des détails via l'API
				KanjiDetails details = api.getKanjiDetails(writing, key, host);
				
				//Conversion vers notre classe locale Kanji simplifiée
				String onyomi = details.getKanji().getOnyomi().getKatakana();
				String kunyomi = details.getKanji().getKunyomi().getHiragana();
				String meaning = details.getKanji().getMeaning().getEnglish();
				
				Kanji K = new Kanji(writing, onyomi, kunyomi, meaning);
				
				base.ajouterKanji(K);

			}

		} catch (ApiException e) {
			System.err.println("Something went wrong...");
			e.printStackTrace();
		}
		
		doGetListerKanji(request, response);
	}
	
	
	protected void doPostSelection(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String s = request.getParameter("selection");
		String a = request.getParameter("answer");
		System.out.println(s);
		System.out.println(a);
		if (s.equals(a)) {
			doGetReponseJuste(request, response);
		}
		else {
			doGetReponseFausse(request, response);			
		}
	}
	
	
	protected void doGetReponseJuste(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String keyHex = request.getParameter("keyHex");
		UTF16HexConversion conv = new UTF16HexConversion(keyHex);
		String key = conv.getUTF16Str();
		System.out.println("Apres conversion, key = " + key);

		String exercise = request.getParameter("exercise");
		if (exercise.equals("writing")) {
			base.increaseScoreWriting(key);
			int nouveauScore = base.getKanji(key).getScoreWriting();
			request.setAttribute("nouveauScore", nouveauScore);
		}
		if (exercise.equals("meaning")) {
			base.increaseScoreMeaning(key);
			int nouveauScore = base.getKanji(key).getScoreMeaning();
			request.setAttribute("nouveauScore", nouveauScore);
		}
		if (exercise.equals("reading")) {
			base.increaseScoreReading(key);
			int nouveauScore = base.getKanji(key).getScoreReading();
			request.setAttribute("nouveauScore", nouveauScore);
		}
		
		request.setAttribute("scoreMax", request.getParameter("scoreMax"));
		request.setAttribute("exercise", exercise);
		request.getRequestDispatcher("CorrectAnswer.jsp").forward(request, response);
	}
	
	
	protected void doGetReponseFausse(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String keyHex = request.getParameter("keyHex");
		UTF16HexConversion conv = new UTF16HexConversion(keyHex);
		String key = conv.getUTF16Str();
		System.out.println("Apres conversion, key = " + key);

		String exercise = request.getParameter("exercise");
		if (exercise.equals("writing")) {
			base.decreaseScoreWriting(key);
			int nouveauScore = base.getKanji(key).getScoreWriting();
			request.setAttribute("nouveauScore", nouveauScore);
		}
		if (exercise.equals("meaning")) {
			base.decreaseScoreMeaning(key);
			int nouveauScore = base.getKanji(key).getScoreMeaning();
			request.setAttribute("nouveauScore", nouveauScore);
		}
		if (exercise.equals("reading")) {
			base.decreaseScoreReading(key);
			int nouveauScore = base.getKanji(key).getScoreReading();
			request.setAttribute("nouveauScore", nouveauScore);
		}
		
		request.setAttribute("scoreMax", request.getParameter("scoreMax"));
		request.setAttribute("exercise", exercise);
		request.getRequestDispatcher("WrongAnswer.jsp").forward(request, response);
	}
}
