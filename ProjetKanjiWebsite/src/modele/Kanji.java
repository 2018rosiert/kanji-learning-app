package modele;
import java.io.UnsupportedEncodingException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "kanjis")

//L'annotation @NamedQuery permet de d�finir des requ�tes nomm�es
@NamedQuery(name = "Kanji.findAll", query = "SELECT k FROM Kanji k")
@NamedQuery(name = "Kanji.findByWriting", query = "SELECT k FROM Kanji k WHERE k.writing = :Writing")
@NamedQuery(name = "Kanji.findByScoreWriting", query = "SELECT k FROM Kanji k WHERE k.scoreWriting <= :scoreWriting")
@NamedQuery(name = "Kanji.findByScoreMeaning", query = "SELECT k FROM Kanji k WHERE k.scoreMeaning <= :scoreMeaning")
@NamedQuery(name = "Kanji.findByScoreReading", query = "SELECT k FROM Kanji k WHERE k.scoreReading <= :scoreReading")

public class Kanji {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "Writing")
	private String writing;	// caractere

	@Column(name = "Onyomi")
	private String onyomi;	// lecture chinoise
	
	@Column(name = "Kunyomi")
	private String kunyomi; // lecture japonaise
	
	@Column(name = "Meaning")
	private String meaning;
	
	@Column(name = "scoreWriting")
	private int scoreWriting;
	
	@Column(name = "scoreMeaning")
	private int scoreMeaning;
	
	@Column(name = "scoreReading")
	private int scoreReading;	

	public String getWriting() {
		return writing;
	}

	public String getOnyomi() {
		return onyomi;
	}

	public String getKunyomi() {
		return kunyomi;
	}

	public String getMeaning() {
		return meaning;
	}
	
	public void setWriting(String writing) {
		this.writing = writing;
	}

	public void setOnyomi(String onyomi) {
		this.onyomi = onyomi;
	}

	public void setKunyomi(String kunyomi) {
		this.kunyomi = kunyomi;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	public int getScoreWriting() {
		return scoreWriting;
	}

	public void increaseScoreWriting() {
		if (this.scoreWriting < 5) {
			this.scoreWriting++;
		}
	}

	public void decreaseScoreWriting() {
		if (this.scoreWriting > 1) {
			this.scoreWriting--;
		}
	}

	public int getScoreMeaning() {
		return scoreMeaning;
	}

	public void increaseScoreMeaning() {
		if (this.scoreMeaning < 5) {
			this.scoreMeaning++;
		}
	}

	public void decreaseScoreMeaning() {
		if (this.scoreMeaning > 1) {
			this.scoreMeaning--;
		}
	}

	public int getScoreReading() {
		return scoreReading;
	}

	public void increaseScoreReading() {
		if (this.scoreReading < 5) {
			this.scoreReading++;
		}
	}

	public void decreaseScoreReading() {
		if (this.scoreReading > 1) {
			this.scoreReading--;
		}
	}

	public Kanji() {
		this.scoreMeaning = 3;
		this.scoreReading = 3;
		this.scoreWriting = 3;
	}

	public Kanji(String writing, String onyomi, String kunyomi, String meaning) throws UnsupportedEncodingException {
		super();
		this.writing = writing;
		this.onyomi = onyomi;
		this.kunyomi = kunyomi;
		this.meaning = meaning;
		this.scoreMeaning = 3;
		this.scoreReading = 3;
		this.scoreWriting = 3;
	}

	@Override
	public String toString() {
		return "Kanji [writing=" + writing + ", onyomi=" + onyomi + ", kunyomi=" + kunyomi + ", meaning=" + meaning
				+ "]";
	}
	
}
