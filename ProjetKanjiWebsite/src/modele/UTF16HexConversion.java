package modele;

import java.io.UnsupportedEncodingException;

public class UTF16HexConversion {
	private String srcStr;
	int i = 0;
	
	public UTF16HexConversion(String srcStr) {
		super();
		this.srcStr = srcStr;
	}
	
	public String getHexStr() throws UnsupportedEncodingException {
	    byte[] bytes = srcStr.getBytes("UTF-16BE");
	
	    StringBuilder sb = new StringBuilder();

	    for (byte b : bytes) {
	    	if (i%2==0) {
	    		sb.append("&#x");
	    	}
	    	sb.append(String.format("%02X", b));
	    	i++;
	    	if (i%2==0) {
	    		sb.append(";");
	    	}
	    }
	   
	    return sb.toString();
	}
	
	public String getUTF16Str() throws UnsupportedEncodingException {
		srcStr = srcStr.replace("&#x", "").replace(";","");
		
		int l = srcStr.length();
		byte[] bytes = new byte[l/2];
	    for (int i = 0; i < l; i += 2) {
	        bytes[i / 2] = (byte) ((Character.digit(srcStr.charAt(i), 16) << 4)
	                + Character.digit(srcStr.charAt(i + 1), 16));
	    }

	    String utf16 = new String(bytes,"UTF-16BE");
	    return utf16;
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		UTF16HexConversion convaller = new UTF16HexConversion("月,;月");
		System.out.println(convaller.getHexStr());
		UTF16HexConversion convretour = new UTF16HexConversion(convaller.getHexStr());
		System.out.println(convretour.getUTF16Str());
	}
	
}
