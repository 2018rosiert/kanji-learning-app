<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Page d'acceuil</title>
<link rel="stylesheet" type="text/css" href="<c:url value='/css/style1.css' />"/>
</head>
<body>
	<div id="menu">
		<p>
			<a href="<c:url value="/listerContact"/>">Lister les contacts</a>
		</p>
		<p>
			<a href="<c:url value="/creerContact"/>">Cr�er un contact</a>
		</p>
	</div>
</body>
</html>