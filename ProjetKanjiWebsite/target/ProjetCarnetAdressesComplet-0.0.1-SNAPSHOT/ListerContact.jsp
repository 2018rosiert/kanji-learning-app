<%-- 
    Document   : ContactInfo
    Created on : 22 juin 2020, 10:04:45
    Author     : ianotto
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>



<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/style2.css" type="text/css" />
</head>
<body>
	<h1>Liste de contacts</h1>
	<table border="1">
		<th>ID</th>
		<th>Nom</th>
		<th>Prenom</th>
		<th>Adresse</th>
		<th>Téléphone</th>
		<c:forEach items="${TousLesContacts}" var="c">
			<tr>
				<td>${c.id}</td>
				<td>${c.nom}</td>
				<td>${c.prenom}</td>
				<td>${c.adresse}</td>
				<td>${c.telephone}</td>
				<td><a href="<c:url value="modifierContact?id=${c.id}"/>">Modifier</a>
				</td>
				<td><a href="<c:url value="supprimerContact?id=${c.id}"/>">Supprimer</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	<a href="<c:url value="/Accueil.jsp"/>">Retour à la page d'accueil</a>
</body>
</html>