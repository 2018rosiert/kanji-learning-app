This git repository contains the local database, two eclipse projects and the slides from the presentation.

Precautions :

1. Chinese characters are well displayed on the html pages but your console might display "?" instead of characters if you do not select UTF-8 as encoder in Eclipse settings.

2. Our database runs on Wampserver. If you don't want to have to edit the persistence.xml file, be sure to have a user named "root" without password but ALL PRIVILEGES. Create a database called kanjis and import the table kanji.sql given in the git repository.

3. We have been struggling with importation of external libraries with Tomcat. Just adding the .jar file to your Build path isn't working. Maybe there is a cleaner solution, but here's the one we used and that worked : put the Kanji-Alive-API-jar-with-dependencies.jar directly in Programmes/Apache_Software_Foundation/Tomcat9.0/lib.

4. A video demo is available in the last slide of the presentation if you can't make it work on your device.

