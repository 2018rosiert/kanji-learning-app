openapi: 3.0.0
info:
  title: "Informations Kanji Alive"
  version: "1"
  description: "Récupérer des listes de Kanji ou des informations sur ceux-ci (écriture, kunyomi, etc)."
servers:
  - url: "https://kanjialive-api.p.rapidapi.com/api/public"
paths:
  /search/advanced:
    get:
      tags:
      - KanjiAlive
      description: "Get the list of 20 Kanjis from a level"
      operationId: "getKanjiList"
      parameters:
        - name: "list"
          in: query
          required: true
          description: "to get a list of kanji from the selected level, verify in the code after"
          schema:
            type: string
        - in: header
          name: "x-rapidapi-key"
          schema:
            type: string
            default: "f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee"
        - in: header
          name: "x-rapidapi-host"
          schema:
            type: string
            default: "kanjialive-api.p.rapidapi.com"
      responses:
        "200":
          description: "Successfull listing of 20 Kanji"
          content:
            application/json:
              schema:
                type: array
                items: {"$ref": "#/components/schemas/kanjiObject"}
        "404":
          description: "No kanji available for that level."
        default:
          description: Default error sample response
  /kanji/{character}:
    get:
      tags:
      - KanjiAlive
      description: "Get a kanji by character"
      operationId: "getKanjiDetails"
      parameters:
        - in: path
          name: character
          required: true
          description: "to get the details about one kanji"
          schema:
            type: string
        - in: header
          name: "x-rapidapi-key"
          schema:
            type: string
            default: "f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee"
        - in: header
          name: "x-rapidapi-host"
          schema:
            type: string
            default: "kanjialive-api.p.rapidapi.com"
      responses:
        "200":
          description: "Details about a kanji"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/kanjiDetails"
        "404":
          description: "Didn't find any kanjis with that code"
        default:
          description: Default error sample response

components:
  schemas:
    kanjiObject:
      type: object
      properties:
        kanji:
          type: object
          properties:
            character:
              type: string
            stroke:
              type: integer
        radical:
          type: object
          properties:
            character:
              type: string
            stroke:
              type: integer
            order:
              type: integer
    kanjiDetails:
      type: object
      properties:
        kanji:
          type: object
          properties:
            character:
              type: string
            meaning:
              type: object
              properties:
                english:
                  type: string
            strokes:
              type: object
              properties: 
                count:
                  type: integer
                timings:
                  type: array
                  items:
                    type: number
                images:
                  type: array
                  items:
                    type: string
                    format: uri              
            onyomi:
              type: object
              properties:
                romaji:
                  type: string
                katakana:
                  type: string
            kunyomi:
              type: object
              properties:
                romaji:
                  type: string
                hiragana:
                  type: string
            video:
              type: object
              properties:
                poster:
                  type: string
                  format: uri
                mp4:
                  type: string
                  format: uri
                webm:
                  type: string
                  format: uri
        radical:
          type: object
          properties:
            character:
              type: string
            strokes:
              type: integer
            image:
              type: string
              format: uri
            position:
              type: object
              properties:
                hiragana:
                  type: string
                romaji:
                  type: string
                icon:
                  type: string
                  format: uri
            name:
              type: object
              properties:
                hiragana:
                  type: string
                romaji:
                  type: string
            meaning:
              type: object
              properties:
                english:
                  type: string
            animation:
              type: array
              items:
                type: string
                format: uri
        references:
          type: object
          properties:
            grade:
              type: integer
            kodansha:
              type: string
              format: color
            classic_nelson:
              type: string
              format: utc-millisec
        examples:
          type: array
          items:
            type: object
            properties:
              japanese:
                type: string
              meaning:
                type: object
                properties:
                  english:
                    type: string
              audio:
                type: object
                properties:
                  opus:
                    type: string
                    format: uri
                  aac:
                    type: string
                    format: uri
                  ogg:
                    type: string
                    format: uri
                  mp3:
                    type: string
                    format: uri