# KanjiAliveApi

All URIs are relative to *https://kanjialive-api.p.rapidapi.com/api/public*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getKanjiDetails**](KanjiAliveApi.md#getKanjiDetails) | **GET** /kanji/{character} | 
[**getKanjiList**](KanjiAliveApi.md#getKanjiList) | **GET** /search/advanced | 


<a name="getKanjiDetails"></a>
# **getKanjiDetails**
> KanjiDetails getKanjiDetails(character, xRapidapiKey, xRapidapiHost)



Get a kanji by character

### Example
```java
// Import classes:
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.Configuration;
import org.openapitools.client.models.*;
import org.openapitools.client.api.KanjiAliveApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://kanjialive-api.p.rapidapi.com/api/public");

    KanjiAliveApi apiInstance = new KanjiAliveApi(defaultClient);
    String character = "character_example"; // String | to get the details about one kanji
    String xRapidapiKey = "\"f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee\""; // String | 
    String xRapidapiHost = "\"kanjialive-api.p.rapidapi.com\""; // String | 
    try {
      KanjiDetails result = apiInstance.getKanjiDetails(character, xRapidapiKey, xRapidapiHost);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling KanjiAliveApi#getKanjiDetails");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **character** | **String**| to get the details about one kanji |
 **xRapidapiKey** | **String**|  | [optional] [default to &quot;f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee&quot;]
 **xRapidapiHost** | **String**|  | [optional] [default to &quot;kanjialive-api.p.rapidapi.com&quot;]

### Return type

[**KanjiDetails**](KanjiDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Details about a kanji |  -  |
**404** | Didn&#39;t find any kanjis with that code |  -  |
**0** | Default error sample response |  -  |

<a name="getKanjiList"></a>
# **getKanjiList**
> List&lt;KanjiObject&gt; getKanjiList(list, xRapidapiKey, xRapidapiHost)



Get the list of 20 Kanjis from a level

### Example
```java
// Import classes:
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.Configuration;
import org.openapitools.client.models.*;
import org.openapitools.client.api.KanjiAliveApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://kanjialive-api.p.rapidapi.com/api/public");

    KanjiAliveApi apiInstance = new KanjiAliveApi(defaultClient);
    String list = "list_example"; // String | to get a list of kanji from the selected level, verify in the code after
    String xRapidapiKey = "\"f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee\""; // String | 
    String xRapidapiHost = "\"kanjialive-api.p.rapidapi.com\""; // String | 
    try {
      List<KanjiObject> result = apiInstance.getKanjiList(list, xRapidapiKey, xRapidapiHost);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling KanjiAliveApi#getKanjiList");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list** | **String**| to get a list of kanji from the selected level, verify in the code after |
 **xRapidapiKey** | **String**|  | [optional] [default to &quot;f4105f3eefmsh5ce550dfc18b27bp153cc1jsn15dc670f9cee&quot;]
 **xRapidapiHost** | **String**|  | [optional] [default to &quot;kanjialive-api.p.rapidapi.com&quot;]

### Return type

[**List&lt;KanjiObject&gt;**](KanjiObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfull listing of 20 Kanji |  -  |
**404** | No kanji available for that level. |  -  |
**0** | Default error sample response |  -  |

