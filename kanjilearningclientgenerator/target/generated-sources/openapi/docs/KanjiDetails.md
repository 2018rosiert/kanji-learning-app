

# KanjiDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kanji** | [**KanjiDetailsKanji**](KanjiDetailsKanji.md) |  |  [optional]
**radical** | [**KanjiDetailsRadical**](KanjiDetailsRadical.md) |  |  [optional]
**references** | [**KanjiDetailsReferences**](KanjiDetailsReferences.md) |  |  [optional]
**examples** | [**List&lt;KanjiDetailsExamples&gt;**](KanjiDetailsExamples.md) |  |  [optional]



