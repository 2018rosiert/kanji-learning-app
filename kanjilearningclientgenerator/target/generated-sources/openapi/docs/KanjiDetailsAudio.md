

# KanjiDetailsAudio

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**opus** | **URI** |  |  [optional]
**aac** | **URI** |  |  [optional]
**ogg** | **URI** |  |  [optional]
**mp3** | **URI** |  |  [optional]



