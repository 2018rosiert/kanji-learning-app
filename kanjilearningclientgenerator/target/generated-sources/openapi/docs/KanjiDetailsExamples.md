

# KanjiDetailsExamples

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**japanese** | **String** |  |  [optional]
**meaning** | [**KanjiDetailsKanjiMeaning**](KanjiDetailsKanjiMeaning.md) |  |  [optional]
**audio** | [**KanjiDetailsAudio**](KanjiDetailsAudio.md) |  |  [optional]



