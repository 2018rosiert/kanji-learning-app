

# KanjiDetailsKanji

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character** | **String** |  |  [optional]
**meaning** | [**KanjiDetailsKanjiMeaning**](KanjiDetailsKanjiMeaning.md) |  |  [optional]
**strokes** | [**KanjiDetailsKanjiStrokes**](KanjiDetailsKanjiStrokes.md) |  |  [optional]
**onyomi** | [**KanjiDetailsKanjiOnyomi**](KanjiDetailsKanjiOnyomi.md) |  |  [optional]
**kunyomi** | [**KanjiDetailsKanjiKunyomi**](KanjiDetailsKanjiKunyomi.md) |  |  [optional]
**video** | [**KanjiDetailsKanjiVideo**](KanjiDetailsKanjiVideo.md) |  |  [optional]



