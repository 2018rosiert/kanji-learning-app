

# KanjiDetailsKanjiKunyomi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**romaji** | **String** |  |  [optional]
**hiragana** | **String** |  |  [optional]



