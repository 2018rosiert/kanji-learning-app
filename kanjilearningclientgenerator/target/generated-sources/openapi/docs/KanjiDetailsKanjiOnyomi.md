

# KanjiDetailsKanjiOnyomi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**romaji** | **String** |  |  [optional]
**katakana** | **String** |  |  [optional]



