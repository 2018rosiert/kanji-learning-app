

# KanjiDetailsKanjiStrokes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**timings** | **List&lt;BigDecimal&gt;** |  |  [optional]
**images** | **List&lt;URI&gt;** |  |  [optional]



