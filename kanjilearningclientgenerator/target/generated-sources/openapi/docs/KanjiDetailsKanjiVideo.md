

# KanjiDetailsKanjiVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**poster** | **URI** |  |  [optional]
**mp4** | **URI** |  |  [optional]
**webm** | **URI** |  |  [optional]



