

# KanjiDetailsRadical

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character** | **String** |  |  [optional]
**strokes** | **Integer** |  |  [optional]
**image** | **URI** |  |  [optional]
**position** | [**KanjiDetailsRadicalPosition**](KanjiDetailsRadicalPosition.md) |  |  [optional]
**name** | [**KanjiDetailsRadicalName**](KanjiDetailsRadicalName.md) |  |  [optional]
**meaning** | [**KanjiDetailsKanjiMeaning**](KanjiDetailsKanjiMeaning.md) |  |  [optional]
**animation** | **List&lt;URI&gt;** |  |  [optional]



