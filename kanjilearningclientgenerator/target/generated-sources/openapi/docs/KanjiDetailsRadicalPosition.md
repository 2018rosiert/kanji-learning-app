

# KanjiDetailsRadicalPosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hiragana** | **String** |  |  [optional]
**romaji** | **String** |  |  [optional]
**icon** | **URI** |  |  [optional]



