

# KanjiDetailsReferences

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grade** | **Integer** |  |  [optional]
**kodansha** | **String** |  |  [optional]
**classicNelson** | **String** |  |  [optional]



