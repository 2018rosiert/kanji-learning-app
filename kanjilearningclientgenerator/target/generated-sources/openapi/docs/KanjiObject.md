

# KanjiObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kanji** | [**KanjiObjectKanji**](KanjiObjectKanji.md) |  |  [optional]
**radical** | [**KanjiObjectRadical**](KanjiObjectRadical.md) |  |  [optional]



