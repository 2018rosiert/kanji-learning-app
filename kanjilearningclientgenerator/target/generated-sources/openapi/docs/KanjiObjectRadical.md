

# KanjiObjectRadical

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**character** | **String** |  |  [optional]
**stroke** | **Integer** |  |  [optional]
**order** | **Integer** |  |  [optional]



