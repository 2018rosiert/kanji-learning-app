/*
 * Informations Kanji Alive
 * Récupérer des listes de Kanji ou des informations sur ceux-ci (écriture, kunyomi, etc).
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.openapitools.client.model.KanjiDetailsAudio;
import org.openapitools.client.model.KanjiDetailsKanjiMeaning;

/**
 * KanjiDetailsExamples
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2021-05-28T16:56:44.988401100+02:00[Europe/Paris]")
public class KanjiDetailsExamples {
  public static final String SERIALIZED_NAME_JAPANESE = "japanese";
  @SerializedName(SERIALIZED_NAME_JAPANESE)
  private String japanese;

  public static final String SERIALIZED_NAME_MEANING = "meaning";
  @SerializedName(SERIALIZED_NAME_MEANING)
  private KanjiDetailsKanjiMeaning meaning;

  public static final String SERIALIZED_NAME_AUDIO = "audio";
  @SerializedName(SERIALIZED_NAME_AUDIO)
  private KanjiDetailsAudio audio;


  public KanjiDetailsExamples japanese(String japanese) {
    
    this.japanese = japanese;
    return this;
  }

   /**
   * Get japanese
   * @return japanese
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getJapanese() {
    return japanese;
  }


  public void setJapanese(String japanese) {
    this.japanese = japanese;
  }


  public KanjiDetailsExamples meaning(KanjiDetailsKanjiMeaning meaning) {
    
    this.meaning = meaning;
    return this;
  }

   /**
   * Get meaning
   * @return meaning
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public KanjiDetailsKanjiMeaning getMeaning() {
    return meaning;
  }


  public void setMeaning(KanjiDetailsKanjiMeaning meaning) {
    this.meaning = meaning;
  }


  public KanjiDetailsExamples audio(KanjiDetailsAudio audio) {
    
    this.audio = audio;
    return this;
  }

   /**
   * Get audio
   * @return audio
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public KanjiDetailsAudio getAudio() {
    return audio;
  }


  public void setAudio(KanjiDetailsAudio audio) {
    this.audio = audio;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    KanjiDetailsExamples kanjiDetailsExamples = (KanjiDetailsExamples) o;
    return Objects.equals(this.japanese, kanjiDetailsExamples.japanese) &&
        Objects.equals(this.meaning, kanjiDetailsExamples.meaning) &&
        Objects.equals(this.audio, kanjiDetailsExamples.audio);
  }

  @Override
  public int hashCode() {
    return Objects.hash(japanese, meaning, audio);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class KanjiDetailsExamples {\n");
    sb.append("    japanese: ").append(toIndentedString(japanese)).append("\n");
    sb.append("    meaning: ").append(toIndentedString(meaning)).append("\n");
    sb.append("    audio: ").append(toIndentedString(audio)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

