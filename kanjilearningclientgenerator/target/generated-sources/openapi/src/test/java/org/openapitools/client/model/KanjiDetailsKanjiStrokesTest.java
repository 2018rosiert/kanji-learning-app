/*
 * Informations Kanji Alive
 * Récupérer des listes de Kanji ou des informations sur ceux-ci (écriture, kunyomi, etc).
 *
 * The version of the OpenAPI document: 1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for KanjiDetailsKanjiStrokes
 */
public class KanjiDetailsKanjiStrokesTest {
    private final KanjiDetailsKanjiStrokes model = new KanjiDetailsKanjiStrokes();

    /**
     * Model tests for KanjiDetailsKanjiStrokes
     */
    @Test
    public void testKanjiDetailsKanjiStrokes() {
        // TODO: test KanjiDetailsKanjiStrokes
    }

    /**
     * Test the property 'count'
     */
    @Test
    public void countTest() {
        // TODO: test count
    }

    /**
     * Test the property 'timings'
     */
    @Test
    public void timingsTest() {
        // TODO: test timings
    }

    /**
     * Test the property 'images'
     */
    @Test
    public void imagesTest() {
        // TODO: test images
    }

}
