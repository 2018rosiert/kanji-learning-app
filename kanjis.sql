-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 03 juin 2021 à 10:25
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `kanjis`
--

-- --------------------------------------------------------

--
-- Structure de la table `kanjis`
--

DROP TABLE IF EXISTS `kanjis`;
CREATE TABLE IF NOT EXISTS `kanjis` (
  `Writing` varchar(50) NOT NULL,
  `Onyomi` varchar(50) NOT NULL,
  `Kunyomi` varchar(50) NOT NULL,
  `Meaning` varchar(50) NOT NULL,
  `ScoreWriting` int(11) NOT NULL,
  `ScoreMeaning` int(11) NOT NULL,
  `ScoreReading` int(11) NOT NULL,
  PRIMARY KEY (`Writing`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `kanjis`
--

INSERT INTO `kanjis` (`Writing`, `Onyomi`, `Kunyomi`, `Meaning`, `ScoreWriting`, `ScoreMeaning`, `ScoreReading`) VALUES
('一', 'イチ', 'ひと', 'one', 3, 3, 3),
('右', 'ウ、ユウ', 'みぎ', 'right', 3, 3, 3),
('運', 'ウン', 'はこ', 'carry, move, fortune', 3, 3, 3),
('英', 'エイ', 'n/a', 'distinguished, England', 3, 3, 3),
('駅', 'エキ', 'n/a', 'station', 3, 3, 3),
('医', 'イ', 'n/a', 'medicine, doctor', 3, 3, 3),
('員', 'イン', 'n/a', 'member', 3, 3, 3),
('安', 'アン', 'やす', 'peaceful, inexpensive', 3, 3, 3),
('泳', 'エイ', 'およ', 'swim', 3, 3, 3),
('育', 'イク', 'そだ', 'raise', 3, 3, 3),
('暗', 'アン', 'くら', 'dark, dim', 3, 3, 3),
('雨', 'ウ', 'あめ、あま', 'rain', 3, 3, 3),
('飲', 'イン', 'の', 'drink', 3, 3, 3),
('映', 'エイ', 'うつ、は', 'reflect, project', 3, 3, 3),
('引', 'イン', 'ひ', 'draw, pull', 3, 3, 3),
('以', 'イ', 'も', 'to the ... of, by means of', 3, 3, 3),
('悪', 'アク、オ', 'わる、あ', 'bad, evil', 3, 3, 3),
('意', 'イ', 'n/a', 'mind, meaning, will', 3, 3, 3),
('院', 'イン', 'n/a', 'institution', 3, 3, 3),
('円', 'エン', 'まる', 'circle, yen', 3, 3, 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
